import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Input extends StatelessWidget {
  final String hint;
  const Input(this.hint, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _controller = TextEditingController();
    return SizedBox(
      height: 56,
      child: TextField(
        style: GoogleFonts.poppins(
          color: const Color(0xFF1C1B1E),
          fontSize: 16,
          fontWeight: FontWeight.w400,
        ),
        controller: _controller,
        decoration: InputDecoration(
          border: const OutlineInputBorder(),
          hintText: hint,
          labelText: hint,
          suffixIcon: IconButton(
            onPressed: _controller.clear,
            icon: const Icon(Icons.highlight_off),
          ),
        ),
      ),
    );
  }
}

class InputPassword extends StatelessWidget {
  final String hint;
  const InputPassword(this.hint, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _controller = TextEditingController();
    return SizedBox(
      height: 56,
      child: TextField(
        style: GoogleFonts.poppins(
          color: const Color(0xFF1C1B1E),
          fontSize: 16,
          fontWeight: FontWeight.w400,
        ),
        controller: _controller,
        decoration: InputDecoration(
          border: const OutlineInputBorder(),
          hintText: hint,
          labelText: hint,
          suffixIcon: IconButton(
            onPressed: _controller.clear,
            icon: const Icon(Icons.highlight_off),
          ),
        ),
        obscureText: true,
        enableSuggestions: false,
        autocorrect: false,
      ),
    );
  }
}
