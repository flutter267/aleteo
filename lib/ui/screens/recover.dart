import 'package:flutter/material.dart';

import '../widgets/button.dart';
import '../widgets/input.dart';
import '../widgets/logo.dart';
import 'code.dart';

class RecoverScreen extends StatelessWidget {
  const RecoverScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Recover(),
      backgroundColor: Colors.white,
    );
  }
}

class Recover extends StatelessWidget {
  const Recover({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Padding(
          padding: EdgeInsets.only(top: 156),
          child: Logo(),
        ),
        const Padding(
          padding: EdgeInsets.only(top: 115, left: 50, right: 41),
          child: Input('E-mail'),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 251, left: 126, right: 127),
          child: Button('Enviar código', onClick: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => const CodeScreen(),
              ),
            );
          }),
        ),
      ],
    );
  }
}
