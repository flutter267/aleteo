import 'package:flutter/material.dart';

import '../widgets/button.dart';
import '../widgets/input.dart';
import '../widgets/logo.dart';

class RestoreScreen extends StatelessWidget {
  const RestoreScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Restore(),
      backgroundColor: Colors.white,
    );
  }
}

class Restore extends StatelessWidget {
  const Restore({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Padding(
          padding: EdgeInsets.only(top: 156),
          child: Logo(),
        ),
        const Padding(
          padding: EdgeInsets.only(top: 115, left: 50, right: 41),
          child: InputPassword('Restablece tu contraseña'),
        ),
        const Padding(
          padding: EdgeInsets.only(top: 40, left: 50, right: 41),
          child: InputPassword('Restablece tu contraseña'),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 155, left: 126, right: 127),
          child: Button(
            'Entrar',
            onClick: () {
              Navigator.popUntil(context, ModalRoute.withName('/'));
            },
          ),
        ),
      ],
    );
  }
}
