import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Home(),
      backgroundColor: Colors.white,
    );
  }
}

class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Text(
      'Landing page',
      style: GoogleFonts.poppins(
        color: const Color(0xFF483FF2),
        fontWeight: FontWeight.w400,
        fontSize: 32,
      ),
    ));
  }
}
