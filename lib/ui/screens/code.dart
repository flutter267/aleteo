import 'package:flutter/material.dart';

import '../widgets/button.dart';
import '../widgets/input.dart';
import '../widgets/logo.dart';
import 'restore.dart';

class CodeScreen extends StatelessWidget {
  const CodeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Code(),
      backgroundColor: Colors.white,
    );
  }
}

class Code extends StatelessWidget {
  const Code({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Padding(
          padding: EdgeInsets.only(top: 156),
          child: Logo(),
        ),
        const Padding(
          padding: EdgeInsets.only(top: 115, left: 50, right: 41),
          child: Input('Código'),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 251, left: 126, right: 127),
          child: Button(
            'Continuar',
            onClick: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const RestoreScreen(),
                ),
              );
            },
          ),
        ),
      ],
    );
  }
}
