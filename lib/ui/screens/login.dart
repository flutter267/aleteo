import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../widgets/button.dart';
import '../widgets/input.dart';
import '../widgets/logo.dart';
import 'home.dart';
import 'recover.dart';
import 'register.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Login(),
      backgroundColor: Colors.white,
    );
  }
}

class Login extends StatelessWidget {
  const Login({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Padding(
          padding: EdgeInsets.only(top: 156),
          child: Logo(),
        ),
        const Padding(
          padding: EdgeInsets.only(top: 115, left: 50, right: 41),
          child: Input('E-mail'),
        ),
        const Padding(
          padding: EdgeInsets.only(top: 40, left: 50, right: 41),
          child: InputPassword('Contraseña'),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 11, left: 160, right: 48),
          child: RichText(
            text: TextSpan(
              text: '¿olvidaste tu contraseña?',
              style: GoogleFonts.poppins(
                color: const Color(0xFF483FF2),
                fontSize: 14,
                fontWeight: FontWeight.w500,
              ),
              recognizer: TapGestureRecognizer()
                ..onTap = () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const RecoverScreen(),
                    ),
                  );
                },
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 124, left: 135, right: 127),
          child: Button('Entrar', onClick: () {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                builder: (context) => const HomeScreen(),
              ),
            );
          }),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 54, left: 81, right: 74),
          child: RichText(
            text: TextSpan(
              children: <TextSpan>[
                TextSpan(
                  text: '¿No tienes una cuenta?',
                  style: GoogleFonts.poppins(
                    color: const Color(0xFF1C1B1E),
                    fontSize: 14,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                TextSpan(
                  text: ' Regístrate',
                  style: GoogleFonts.poppins(
                    color: const Color(0xFF483FF2),
                    fontSize: 14,
                    fontWeight: FontWeight.w500,
                  ),
                  recognizer: TapGestureRecognizer()
                    ..onTap = () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => const RegisterScreen(),
                        ),
                      );
                    },
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
