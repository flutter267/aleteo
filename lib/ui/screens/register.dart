import 'package:flutter/material.dart';

import '../widgets/button.dart';
import '../widgets/input.dart';
import '../widgets/logo.dart';

class RegisterScreen extends StatelessWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Register(),
      backgroundColor: Colors.white,
    );
  }
}

class Register extends StatelessWidget {
  const Register({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Padding(
          padding: EdgeInsets.only(top: 156),
          child: Logo(),
        ),
        const Padding(
          padding: EdgeInsets.only(top: 115, left: 50, right: 41),
          child: Input('E-mail'),
        ),
        const Padding(
          padding: EdgeInsets.only(top: 40, left: 50, right: 41),
          child: InputPassword('Contraseña'),
        ),
        const Padding(
          padding: EdgeInsets.only(top: 40, left: 50, right: 41),
          child: InputPassword('Verifique la constraseña'),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 63, left: 132, right: 131),
          child: Button('Registrarme', onClick: () {
            Navigator.pop(context);
          }),
        ),
      ],
    );
  }
}
